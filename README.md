# Prometheus datasource oauth proxy

This solution supports the following flow
1.	Through Auth0 you can manage tenants (users)
2.	They can log in through Oauth integration into Grafana
3.	They can select and view data from a data source
4.	For the data source, Grafana has the option of forwarding Oauth towards the data source 

![Prometheus_Oauth_proxy_flow](https://gitlab.com/optima_public/prometheus_oauth_proxy/-/wikis/uploads/89847d795571967d81bfb4d3f9c288ec/Prometheus_Oauth_proxy_flow.png)

## Docker compose example
For better understanding and local debugging of the configuration is prepared a comprehensive deployment scenario with the help of docker-compose. 
Located in directory

https://gitlab.com/optima_public/prometheus_oauth_proxy/-/tree/master/tests/docker_compose

There is runner.sh, which refers to the official docker builds with tags placed on the

https://gitlab.com/optima_public/prometheus_oauth_proxy/-/tags

Or we can make local changes, and then use runner.local-build.sh
The basic configuration is as follows:
1.	We will create local directories for Victoria metrics data and Grafana data. These directories are a persistent data store. We will use those directory names in runner.sh files as ENV variables.  
2.	Modify docker-compose.yaml if we need a specific version of Grafana, or change ports. ...
3.	The other ENV parameters of Grafana are located in the config file. In principle, it is mainly the password admin USER and OAUTH configuration parameters. Ref. also https://grafana.com/docs/grafana/latest/auth/generic-oauth/ - Set up OAuth2 with Auth0     
4.	The value GF_SERVER_ROOT_URL   represents the address where we want our deployment to be available (localhost may well be), but this must be set on the Auth0 side because of Oauth redirect - user is redirected to this domain after login.
5.	We will also set Auth0 provider according to the Grafana configuration procedure (https://grafana.com/docs/grafana/latest/auth/generic-oauth/  - Set up OAuth2 with Auth0 ) , or we can use another 
6.	The provisioning directory is an example of how to automatically deploy a custom data source (in our case pointing to our proxy) and how to automatically deploy a pre-built dashboards. 
7.	The ENV parameters for the proxy are listed in the config.prometheus_oauth_proxy file. 
8.	Run runner.sh, respectively. runner.local-build.sh
9.	Open Grafana in browser at GF_SERVER_ROOT_URL and log in (locally as admin or Sign in with Auth0)  
10.	Take a look at pre-defined dashboard in folder sample_dashboards
11.	To test the simulated data, we can use data_simulation.py script - https://gitlab.com/optima_public/prometheus_oauth_proxy/-/blob/master/tests/data_simulation.py
in which modify METRICS_SERVER_URL and fill the Victoria-metrics some data. This script is also an example of how it is possible to send multiple metrics in one call. Ref. also https://github.com/VictoriaMetrics/VictoriaMetrics#how-to-send-data-from-influxdb-compatible-agents-such-as-telegraf

Meaning of ENV parameters for proxy is:

| Property | Value |
| --- | --- |
| PROMETHEUS_DATASOURCE |	Address of the original (victoria-metrics) data source http(s)://  In our example ref. docker-compose - service: victoriametrics |
| PROMETHEUS_CUSTOMER_ID_TAG |	This label is automatically added to each series (vector) in the query |
| OAUTH_AUTH0_DOMAIN |	Oauth domain, from which userinfo is obtained  https://_OAUTH_AUTH0_DOMAIN/userinfo |
| OAUTH_CUSTOMER_ID_CLAIM |	Userinfo looks for this parameter that uniquely identifies the tenant and its value is used in PROMETHEUS_CUSTOMER_ID_TAG |
| DEBUG |	"True" - more detailed output to the terminal |
| PYTHONUNBUFFERED |	Set to 0, the solution for immediate output to the terminal in the docker compose |

## Helm chart - prometheus_oauth_proxy
One of the project’s outputs is the helm chart, for easier Kubernetes deployment. There is also repository with Prometheus Oauth proxy helm chart.

**Usage**

Add a chart helm repository

`helm repo add optima_public https://optima_public.gitlab.io/helm_repo/` 

Test the helm repository

`helm search prometheus-oauth-proxy` 

Installing the chart

`helm install -n prometheus-oauth-proxy optima_public/prometheus_oauth_proxy`

Configuration
The following table lists the major configurable parameters of this chart and their default values.

| Parameter |	Description	| Default | 
| --- | --- | --- | 
| PROMETHEUS_DATASOURCE |	Address of the original (victoria-metrics) data source http(s):// For kubernetes, this address need not be exposed publicly. ClusterIP is enough. |	"http://victoriametrics:8428"|
| PROMETHEUS_CUSTOMER_ID_TAG |	This label is automatically added to each series in the query	| "customer_id" |
| OAUTH_AUTH0_DOMAIN |	Oauth domain, from which userinfo is obtained https://_OAUTH_AUTH0_DOMAIN/userinfo | "optimaideas.eu.auth0.com" |
| OAUTH_CUSTOMER_ID_CLAIM | Userinfo looks for this parameter that uniquely identifies the tenant and its value is used in PROMETHEUS_CUSTOMER_ID_TAG |	"http://optimaideas.com/customer_id" |
| DEBUG |	"True" - more detailed output |	"False" |

Specify each parameter using the --set key=value[,key=value] argument to helm install.

Alternatively, a YAML file that specifies the values for the parameters can be provided while installing the chart. 
For example,

`$ helm install -n prometheus_oauth_proxy  -f values.yaml optima_public/prometheus_oauth_proxy`





