FROM python:3.6-alpine
RUN apk add bash
RUN mkdir -p /app/prometheus_oauth/proxy
RUN mkdir -p /app/prometheus_oauth/tests/test_parser
ADD ./requirements.txt /app/prometheus_oauth
ADD ./proxy /app/prometheus_oauth/proxy
ADD ./tests/test_parser /app/prometheus_oauth/tests/test_parser
ENV PYTHONPATH "${PYTHONPATH}:/app/prometheus_oauth"
WORKDIR /app/prometheus_oauth
RUN pip install -r requirements.txt