import logging
from lark import Lark, Transformer, Tree, v_args, Token
from lark.reconstruct import Reconstructor
from lark.exceptions import VisitError
from proxy.utils import AuthError


LOG = logging.getLogger()

promql_grammar = """
    start: instruction~1
    
    instruction: vector                                             -> vector_instruction
               | label_block                                        -> label_matches_instruction
               | function                                           -> function_instruction
               | operation                                          -> operation_instruction

               
    scalar: NUMBER
    CNAME_SPEC: ("_"|":"|"."|LETTER) ("_"|":"|"."|LETTER|DIGIT)*
    CNAME_SPEC2: ("_"|":"|"."|LETTER|DIGIT) ("_"|":"|"."|LETTER|DIGIT)*
    vector: CNAME_SPEC [label_block|range_block]*                                                                   
    label_block: "{" [label_tag ("," label_tag)*] "}"            
    label_tag:   LABEL_NAME LABEL_OPERATION LABEL_VALUE                  
    LABEL_NAME: CNAME_SPEC                                                 
    LABEL_OPERATION: ("!~"|"=~"|"="|"!=")                                         
    LABEL_VALUE: ESCAPED_STRING                                       
    range_block: "[" [range* (":" resolution)*] "]" 
    range: CNAME_SPEC2
    resolution: CNAME_SPEC2
    offset: " offset" duration
    duration: CNAME_SPEC2
    function: FNC_NAME without_by_block "(" parameter_block  (function|vector_offseted|operation)~1 ")" range_block*  without_by_block
    without_by_block: ( WITHOUT_BY~1 "("  label_list ")" )?
    WITHOUT_BY: ("without"|"by") 
    parameter_block: (PARAMETER "," (PARAMETER)*)?
    FNC_NAME: FUNCTION_NAME|FUNCTION_NAME2|FUNCTION_NAME3|FUNCTION_NAME4|AGGREGATION_OVER_TIME|AGGREGATION_OVER_TIME2|AGGREGATION_OPERATOR
    FUNCTION_NAME: ("rate"|"irate"|"increase"|"max_over_time"|"deriv"|"absent"|"abs"|"ceil"|"changes"|"clamp_max"|"clamp_min")
    FUNCTION_NAME2: ("day_of_month"|"day_of_week"|"days_in_month"|"delta"|"exp"|"floor"|"histogram_quantile")
    FUNCTION_NAME3: ("holt_winters"|"hour"|"idelta"|"ln"|"log2"|"log10"|"minute"|"month"|"predict_linear"|"resets"|"round")
    FUNCTION_NAME4: ("scalar"|"sort_desc"|"sort"|"sqrt"|"timestamp"|"time"|"vector"|"year")
    AGGREGATION_OVER_TIME: ("avg_over_time"|"min_over_time"|"max_over_time"|"sum_over_time"|"count_over_time")
    AGGREGATION_OVER_TIME2: ("quantile_over_time"|"stddev_over_time"|"stdvar_over_time")    
    AGGREGATION_OPERATOR: ("sum"|"min"|"max"|"avg"|"stddev"|"stdvar"|"count_values"|"count"|"bottomk"|"topk"|"quantile")
    vector_offseted: [vector|offset]*
    ARITMETIC_OPERATOR: ("+"|"-"|"*"|"/"|"%"|"^")
    COMPARISON_OPERATOR: ("=="|"!="|">"|"<"|">="|"<=")
    LOGICAL_OPERATOR: ("and"|"or"|"unless")
    bin_op: (ARITMETIC_OPERATOR|COMPARISON_OPERATOR|LOGICAL_OPERATOR)
    operation:  (operand|operation|braced_opration) bin_op [ignoring|on] (operand|operation|braced_opration) 
    braced_opration: "(" operation ")"
    operand: (scalar|vector|function)
    ignoring:  "ignoring(" label_list ") "
    on: "on" "(" label_list ") "
    label_list: [LABEL_NAME ("," LABEL_NAME)*] 
    PARAMETER: NUMBER|ESCAPED_STRING  
      
    %import common.ESCAPED_STRING
    %import common.DIGIT
    %import common.LETTER
    %import common.NUMBER
    %import common.WS
    %ignore WS
"""

'''
not supported functions now (potentially dangerous or strange syntax):
    "label_join"
    "label_replace"
    
'''

class Parser():
    def __init__(self):
        self.parser = Lark(promql_grammar)
        self.reconstructor = Reconstructor(self.parser)

    def parse(self, text):
        return self.parser.parse(text)

    def reconstruct(self, tree):
        return self.reconstructor.reconstruct(tree)



class VectorTransformer(Transformer):
    def __init__(self, label_name, whitelisted_vectors = None):
        Transformer.__init__(self)
        self.label_name = label_name
        self.label_value = None
        self.whitelisted_vectors = [] if not whitelisted_vectors else whitelisted_vectors
        parser = Parser()
        tr = parser.parse("{"+label_name + "=\"_nvl_\"}")
        self.template_label_block = tr.children[0].children[0].copy()
        self.template_label = self.template_label_block.children[0].copy()


    @v_args(tree=True)
    def vector(self, tree):
        if len(tree.children) == 0:
            return tree
        if tree.children[0].value in self.whitelisted_vectors:
            return tree
        # check for label block
        has_label_block = False
        for item in tree.children:
            if isinstance(item, Token):
                continue
            if item.data == 'label_block':
                has_label_block = True
                break
        # set label block if not exists
        if not has_label_block:
            nchildren = []
            nchildren.append(tree.children[0])
            nchildren.append(self.template_label_block.copy())
            if len(tree.children) > 1:
                nchildren.extend(tree.children[1:])
            tree.children = nchildren
        # set value of correct label
        label_updated = False
        for item in tree.children:
           if isinstance(item, Token):
               continue
           if item.data == 'label_block':
               for chitem in item.children:
                   if isinstance(chitem, Token):
                       continue
                   if chitem.data == 'label_tag' and chitem.children[0].value == self.label_name:
                        chitem.children[2] = chitem.children[2].update(value=str(self.label_value))
                        label_updated = True
        # if not such label - create one
        if not label_updated:
            nlabel = self.template_label.copy()
            nlabel.children[2] = nlabel.children[2].update(value=str(self.label_value))
            for item in tree.children:
                if isinstance(item, Token):
                    continue
                if item.data == 'label_block':
                    item.children.append(nlabel)
        return tree

    @v_args(tree=True)
    def without_by_block(self, tree):
        if len(tree.children) == 0:
            return tree
        if tree.children[0].value == 'without':
            for label in tree.children[1].children:
                if label.value ==  self.label_name:
                    raise AuthError(f'using {self.label_name} in without clause is denied', 400)
        return tree

    def transform_vector_labels(self, tree, label_value):
        self.label_value = f"\"{label_value}\""
        try:
            rt = super().transform(tree)
        except VisitError as e:
            if isinstance(e.orig_exc, AuthError):
                raise  e.orig_exc
        self.label_value = None
        return rt



class PromqlRewriter():
    def __init__(self, label_name, whitelisted_vectors = None ):
        self.parser = Parser()
        self.trans = VectorTransformer(label_name, whitelisted_vectors=whitelisted_vectors)


    def rewrite(self, promql, label_value):
        try:
            tree = self.parser.parse(promql)
            ntree = self.trans.transform_vector_labels(tree, label_value)
            return self.parser.reconstruct(ntree)
        except AuthError:
            raise
        except Exception as e:
            LOG.error(e)
            raise AuthError('Rewrite exception, processing is denied', 400)



