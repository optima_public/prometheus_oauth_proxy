import logging
from proxy.utils import AuthError
from proxy.parser import PromqlRewriter
from flask import g

LOG = logging.getLogger()

def rewrite_request(path, params, customer_id_tag, customer_id):
    _path = str(path)
    # we deny to get all tennants id's
    if customer_id_tag in path:
        raise AuthError(f'Query of {customer_id_tag} not allowed', 401)
    if _path.lower().endswith('query') or _path.lower().endswith('query_range'):
        if 'query' in params:
            nlist = []
            plist = params.getlist('query')
            for qry in plist:
                LOG.debug(f'original: {qry}')
                qry_rewrited = get_rewriter(customer_id_tag).rewrite(qry, customer_id)
                LOG.debug(f'rewrited: {qry_rewrited}')
                nlist.append(qry_rewrited)
            nparams = params.copy()
            nparams.setlist('query', nlist)
            return nparams
        else:
            LOG.warning('Parameter query is missing')
    elif _path.lower().endswith('series'):
        if 'match[]' in params:
            nlist = []
            plist = params.getlist('match[]')
            for match in plist:
                LOG.debug(f'original: {match}')
                qry_rewrited = get_rewriter(customer_id_tag).rewrite(match, customer_id)
                LOG.debug(f'rewrited: {qry_rewrited}')
                nlist.append(qry_rewrited)
            nparams = params.copy()
            nparams.setlist('match[]', nlist)
            return nparams
        else:
            LOG.warning('Parameter match[] is missing')
    return params


def get_rewriter(customer_id_tag):
    if not hasattr(g, 'promql_rewriter'):
        g.promql_rewriter = PromqlRewriter(label_name=customer_id_tag)
        LOG.debug('Created promql_rewriter')
    return g.promql_rewriter

