from flask import Flask,request, Response
import requests
import os
import logging
from proxy.utils import get_from_env, get_customer_id, setup_log, AuthError
from proxy.rewriter import rewrite_request

LOG = logging.getLogger()

app = Flask(__name__)
app.secret_key = os.urandom(24)

PORT =  get_from_env('PORT', '18428')
DEBUG = get_from_env('DEBUG', 'False')
PROMETHEUS_DATASOURCE = get_from_env('PROMETHEUS_DATASOURCE')
PROMETHEUS_CUSTOMER_ID_TAG = get_from_env('PROMETHEUS_CUSTOMER_ID_TAG','customer_id')
OAUTH_AUTH0_DOMAIN =  get_from_env('OAUTH_AUTH0_DOMAIN')
OAUTH_CUSTOMER_ID_CLAIM =  get_from_env('OAUTH_CUSTOMER_ID_CLAIM','http://optimaideas.com/customer_id')


def request_trace(msg, path, params,  tennant_id):
    LOG.debug(f'{msg} path={path} args={params} tennant_id={tennant_id}')

@app.route('/')
def index():
    return '...'

@app.route('/<path:path>',methods=['GET'])
def proxy(path):
    global PROMETHEUS_DATASOURCE, OAUTH_CUSTOMER_ID_CLAIM, OAUTH_AUTH0_DOMAIN
    try:
        params = request.args
        customer_id = get_customer_id(OAUTH_AUTH0_DOMAIN, OAUTH_CUSTOMER_ID_CLAIM)
        # customer_id = '200'
        request_trace('Before rewrite', path, params, customer_id)
        nparams = rewrite_request(path, params,PROMETHEUS_CUSTOMER_ID_TAG, customer_id)
        request_trace('After rewrite', path, nparams, customer_id)
        resp = requests.get(f'{PROMETHEUS_DATASOURCE}{path}', params=nparams)
        excluded_headers = ['content-encoding', 'content-length', 'transfer-encoding', 'connection']
        headers = [(name, value) for (name, value) in  resp.raw.headers.items() if name.lower() not in excluded_headers]
        return Response(resp.content, resp.status_code, headers)
    except AuthError as ae:
        LOG.error(ae)
        return ae.error, ae.status_code
    except Exception as e:
        LOG.error(e, exc_info=True)
        return "Processing error", 503


if __name__ == '__main__':
    # small fixes
    if not PROMETHEUS_DATASOURCE.endswith('/'):
        PROMETHEUS_DATASOURCE = PROMETHEUS_DATASOURCE + '/'
    if not isinstance(DEBUG, bool):
        DEBUG = str(DEBUG).lower() == 'true'
    setup_log(DEBUG)
    app.run(debug = DEBUG, port= int(PORT), host='0.0.0.0')
