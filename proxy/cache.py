from time import time
import itertools
from threading import RLock

DEFAULT_TTL = 120


class CacheEntry():
    def __init__(self, data, ttl=DEFAULT_TTL):
        self.data = data
        self.expires_at = time() + ttl
        self._expired = False

    def expired(self):
        if self._expired is False:
            return (self.expires_at < time())
        else:
            return self._expired


class Cache():
    def __init__(self):
        self.entries = {}
        self.lock = RLock()

    def put(self, key, data, ttl=DEFAULT_TTL):
        with self.lock:
            self.entries[key] = CacheEntry(data, ttl)

    def get(self, key):
        with self.lock:
            if not key in self.entries:
                return None
            entry = self.entries[key]
            if entry.expired():
                self.entries.pop(key)
                return None
            return entry.data

