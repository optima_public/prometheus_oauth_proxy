import os
import sys
import json
import logging
import http.client
from flask import request
from proxy.cache import Cache


def get_from_env(vname, vdefault = None):
    if vname in os.environ:
        return os.environ[vname]
    else:
        if not vdefault:
            raise Exception(f'{vname} not defined in env')
        else:
            return vdefault


class _MaxLevelFilter(object):
    def __init__(self, highest_log_level):
        self._highest_log_level = highest_log_level

    def filter(self, log_record):
        return log_record.levelno <= self._highest_log_level

def setup_log(_debug = False):
    log_type = logging.DEBUG if _debug else  logging.INFO
    logger = logging.getLogger()
    logger.setLevel(log_type)
    log_out = logging.StreamHandler(sys.stdout)
    log_out.addFilter(_MaxLevelFilter(logging.WARNING))
    log_err = logging.StreamHandler(sys.stderr)
    log_err.setLevel(logging.ERROR)
    logger.addHandler(log_err)
    log_out.setLevel(log_type)
    log_out.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s - %(filename)s.%(funcName)s %(message)s'))
    logger.addHandler(log_out)


# Format error response and append status code.
class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


def get_token_auth_header():
    """Obtains the access token from the Authorization Header
    """
    auth = request.headers.get("Authorization", None)
    if not auth:
        raise AuthError("Authorization header is expected", 401)

    parts = auth.split()

    if parts[0].lower() != "bearer":
        raise AuthError( "Authorization header must start with Bearer", 401)
    elif len(parts) == 1:
        raise AuthError("Token not found", 401)
    elif len(parts) > 2:
        raise AuthError("Authorization header must be Bearer token", 401)

    token = parts[1]
    return token

auth_cache = Cache()

def get_customer_id(_OAUTH_AUTH0_DOMAIN, _OAUTH_CUSTOMER_ID_CLAIM):
    global auth_cache
    token = get_token_auth_header()
    session_key = f'{_OAUTH_CUSTOMER_ID_CLAIM}_{token}'
    val = auth_cache.get(session_key)
    if val:
        return val

    conn = http.client.HTTPSConnection(_OAUTH_AUTH0_DOMAIN)
    headerz = {
        'authorization': f"Bearer {token}"
    }
    conn.request("GET", url='/userinfo', headers=headerz)
    resp = conn.getresponse()
    if resp.status != 200:
        raise AuthError(f'There is a problem with Auto0 {resp.status}', 401)
    jdata = json.loads(resp.read().decode(resp.headers.get_content_charset()))
    if _OAUTH_CUSTOMER_ID_CLAIM not in jdata:
        raise AuthError(f'Customer id claim {_OAUTH_CUSTOMER_ID_CLAIM} not is userinfo. Seems Auth0 is not configured (setup rule for that).', 401)
    auth_cache.put(session_key,jdata[_OAUTH_CUSTOMER_ID_CLAIM])
    return auth_cache.get(session_key)