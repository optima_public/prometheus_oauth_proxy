import random
import requests
import time
import re

METRICS_SERVER_URL = 'http://demo.aicu.optimaideas.com:8428/write'

METRICS = [
    ("coordinator.customer.jobs.started", 0,3),
    ("coordinator.customer.jobs.waiting", 0,3),
    ("coordinator.job.worker.missing", 0,5),
    ("parking.image_input.bytes_read",0,500000),
    ("parking.image_input.download.time", 0.1, 2.5),
    ("parking.checker.check_place.time", 0.2, 0.5),
    ("parking.checker.check_image.time", 5, 25),
    ("parking.checker.places_all", 54, 55),
    ("parking.checker.places_free", 0, 25),
    ("parking.notifier.mqtt_messages_sent", 0, 55)
]

PROMETHEUS_CUSTOMER_ID_TAG = "customer_id"
PROMETHEUS_JOB_ID_TAG = "job_id"

TENNANTS = [
    {
        'id': "100",
        'jobs': ['Parking Bratislava 1', 'Parking Vienna', 'Parking Bratislava UK']
    },
    {
        'id': "200",
        'jobs': ['Place house 1', 'Parking own property', 'Camera at home', 'Hotel Berlin 55']
    },
    {
        'id': "1999",
        'jobs': ['Doma a na ulici', 'U Jozefa číslo 12', 'Test ľščťžýáíéúäô  " \' =-{} ()']
    }
]


def fix_string(s):
    return re.sub(r"[,= ]", r"\\\g<0>", s)


# output format   <tags, >  <metric1=value, metric2=value ...>

def make_random_measurement():
    # take random number of metrics
    mcount = random.randrange(1,len(METRICS))
    tennant = random.randrange(len(TENNANTS))
    job_id = fix_string(TENNANTS[tennant]['jobs'][random.randrange(len(TENNANTS[tennant]['jobs']))])
    tennant_id = TENNANTS[tennant]['id']
    tags = f'{PROMETHEUS_JOB_ID_TAG}={job_id},{PROMETHEUS_CUSTOMER_ID_TAG}={tennant_id}'
    mlist = []
    unique = {}
    for x in range(mcount):
        rndpos = random.randrange(len(METRICS))
        if rndpos in unique:
            continue
        unique[rndpos] = rndpos
        (_name, _min, _max) = METRICS[rndpos]
        val = 0
        if isinstance(_max, float):
            val = random.uniform(_min,_max)
        else:
            val = random.randrange(_min,_max)
        mlist.append(f'{fix_string(_name)}={val}')
    metrics = ','.join(mlist)
    return f',{tags} {metrics}'



def send_measurement(mdata):
    print(f'sending: {mdata}')
    resp = requests.post(METRICS_SERVER_URL,mdata.encode('utf-8'))
    if resp.status_code != 204:
        print(f'There are some problems {resp.status_code}')



if __name__ == '__main__':
    while True:
        msmts = []
        for x in range(random.randrange(1,20)):
            msmts.append(make_random_measurement())
        send_measurement('\n'.join(msmts))
        time.sleep(random.uniform(4,10))

