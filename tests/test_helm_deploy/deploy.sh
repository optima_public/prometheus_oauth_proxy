#!/usr/bin/env bash

set -e

export CI_COMMIT_TAG=0.2.3


helm dependency update ../../helm/prometheus-oauth-proxy
../../helm/substitute_version.sh
helm package --version $CI_COMMIT_TAG --app-version $CI_COMMIT_TAG -d ../../helm/  ../../helm/prometheus-oauth-proxy/
helm upgrade --install    --force prometheus-oauth-proxy  ../../helm/prometheus-oauth-proxy-$CI_COMMIT_TAG.tgz
helm ls