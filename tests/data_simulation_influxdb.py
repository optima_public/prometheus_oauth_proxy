import random
import requests
import time

from influxdb import InfluxDBClient

# 'influxdb://username:password@ocalhost:8086/databasename', timeout=5)
INFLUXDB_DSN = 'influxdb://demo.aicu.optimaideas.com:8428'

METRICS = [
    ("coordinator.customer.jobs.started", 0,3),
    ("coordinator.customer.jobs.waiting", 0,3),
    ("coordinator.job.worker.missing", 0,5),
    ("parking.image_input.bytes_read",0,500000),
    ("parking.image_input.download.time", 0.1, 2.5),
    ("parking.checker.check_place.time", 0.2, 0.5),
    ("parking.checker.check_image.time", 5, 25),
    ("parking.checker.places_all", 54, 55),
    ("parking.checker.places_free", 0, 25),
    ("parking.notifier.mqtt_messages_sent", 0, 55)
]

PROMETHEUS_CUSTOMER_ID_TAG = "customer_id"
PROMETHEUS_JOB_ID_TAG = "job_id"

TENNANTS = [
    {
        'id': "100",
        'jobs': ['Parking Bratislava 1', 'Parking Vienna', 'Parking Bratislava UK']
    },
    {
        'id': "200",
        'jobs': ['Place house 1', 'Parking own property', 'Camera at home', 'Hotel Berlin 55']
    },
    {
        'id': "1999",
        'jobs': ['Doma a na ulici', 'U Jozefa číslo 12', 'Test ľščťžýáíéúäô  " \' =-{} ()']
    }
]


def make_random_measurement():
    measurement = {
        #"measurement": "m1",
        "tags": {
        },
        "fields": {

        },
        "time": int(time.time() * 1000)
    }

    # take random number of metrics
    mcount = random.randrange(1,len(METRICS))
    tennant = random.randrange(len(TENNANTS))
    job_id = TENNANTS[tennant]['jobs'][random.randrange(len(TENNANTS[tennant]['jobs']))]
    tennant_id = TENNANTS[tennant]['id']

    measurement['tags'][PROMETHEUS_JOB_ID_TAG] = job_id
    measurement['tags'][PROMETHEUS_CUSTOMER_ID_TAG] = tennant_id

    unique = {}
    for x in range(mcount):
        rndpos = random.randrange(len(METRICS))
        if rndpos in unique:
            continue
        unique[rndpos] = rndpos
        (_name, _min, _max) = METRICS[rndpos]
        val = 0
        if isinstance(_max, float):
            val = random.uniform(_min,_max)
        else:
            val = random.randrange(_min,_max)

        measurement['fields'][_name] = val
    return measurement



if __name__ == '__main__':
    cli = InfluxDBClient.from_dsn(INFLUXDB_DSN, timeout=5)
    while True:
        points = []
        for x in range(random.randrange(1,20)):
            points.append(make_random_measurement())

        print('written={}'.format(cli.write_points(points=points,time_precision='ms')))
        time.sleep(random.uniform(2,8))

