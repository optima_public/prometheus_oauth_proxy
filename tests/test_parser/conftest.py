import pytest
from proxy.parser import Parser, PromqlRewriter
from tests.test_parser.test_parser import whitelist

@pytest.fixture(scope="module")
def parser():
    parser = Parser()
    yield  parser


@pytest.fixture(scope="module")
def rewriter():
    rewriter = PromqlRewriter('client_id')
    yield  rewriter


@pytest.fixture(scope="module")
def whitelisted_rewriter():
    whitelisted_rewriter = PromqlRewriter('client_id', whitelisted_vectors=whitelist)
    yield  whitelisted_rewriter