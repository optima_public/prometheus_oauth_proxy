import pytest
from proxy.parser import Parser
from proxy.utils import AuthError

promql_samples = [
    '{__name__=~"job:.*"}',
    'http_requests_total',
    'http_requests_total{job="prometheus",code="200"}',
    'http_requests_total{status_code=~"2.*"}',
    'http_requests_total{client_id="123-245-zzzsss_1999",job_id="romantic_lovelace_87f7387d-da32"}',
    'http_requests_total[5h]',
    'http_requests_total[5m]',
    'http_requests_total{status_code=~"2.*"}[50h]',
    'http_requests_total{job="apiserver",handler="/api/comments"}',
    'http_requests_total{job="apiserver",handler="/api/comments"}[5m]',
    'http_requests_total{job=~".*server"}',
    'http_requests_total{status!~"4.."}',
    'rate(http_requests_total[5m])',
    'irate(http_requests_total[5m])',
    'increase(http_requests_total{status_code=~"2.*"})',
    'sum(rate(http_requests_total[5m]))',
    'sum(rate(http_requests_total[5m] offset 5m))',
    'rate(http_requests_total[5m])[30m:1m]',
    'max_over_time(deriv(rate(distance_covered_total[5s])[30s:5s])[10m:])',
    'sum(http_requests_total{method="GET"} offset 5m)',
    '100*100',
    '12.1*http_requests_total[1h]',
    'http_requests_total[5h]*http_requests_total[1h]',
    '100>100',
    '12.1!=http_requests_total[1h]',
    'http_requests_total[5h]==http_requests_total{status_code=~"2.*"}[50h]',
    'node_filesystem_avail{fstype!~"tmpfs|fuse.lxcfs|squashfs"}/node_filesystem_size{fstype!~"tmpfs|fuse.lxcfs|squashfs"}',
    'rate(http_requests_total{status_code=~"5.*"}[5m])/rate(http_requests_total[5m])',
    'rate(http_requests_total{status_code=~"5.*"}[5m])>.1*rate(http_requests_total[5m])',
    'max_over_time(deriv(rate(distance_covered_total[5s])[30s:5s])[10m:])and http_requests_total{status_code=~"2.*"}[50h]',
    'method_code:http_errors:rate5m{code="500"}',
    'method_code.http_errors.rate5m{code.isgood="500"}',
    'method_code:http_errors:rate5m{code="500"}/ignoring(code) method:http_requests:rate5m',
    'sum without(instance)(http_requests_total)',
    'sum by(application,group)(http_requests_total)',
    'sum(http_requests_total)',
    'count_values("version",build_version)',
    'topk(5,http_requests_total)',
    'sum(http_requests_total)by(application,group)',
    'sum(http_requests_total)without(instance)',
    'absent(sum(nonexistent{job="myjob"}))',
    'delta(cpu_temp_celsius{host="zeus"}[2h])',
    'histogram_quantile(0.9,rate(http_request_duration_seconds_bucket[10m]))',
    'histogram_quantile(0.9,sum(rate(http_request_duration_seconds_bucket[10m]))by(job,le))',
    'histogram_quantile(0.9,sum(rate(http_request_duration_seconds_bucket[10m]))by(le))',
    'increase(http_requests_total{job="api-server"}[5m])',
    'irate(http_requests_total{job="api-server"}[5m])',
    'topk(3,sum by(app,proc)(rate(instance_cpu_time_ns[5m])))',
    '(instance_memory_limit_bytes-instance_memory_usage_bytes)/1024/1024',
    'sum by(app,proc)(instance_memory_limit_bytes-instance_memory_usage_bytes)/1024/1024',
    'sum by(status_code)(rate(http_requests_total[5m]))',
    '100*(1-avg by(instance)(irate(node_cpu{mode="idle"}[5m])))',
    'sum(sort_desc(sum_over_time(ALERTS{alertstate="firing"}[24h])))by(alertname)'
]


rewriter_samples = [
    ('{__name__=~"job:.*"}','{__name__=~"job:.*"}'),
    ('http_requests_total', 'http_requests_total{client_id="1999"}'),
    ('http_requests_total{client_id="19sadf99",client_id="19",client_id="1"}', 'http_requests_total{client_id="1999",client_id="1999",client_id="1999"}'),
    ('http_requests_total{job="prometheus",code="200"}','http_requests_total{job="prometheus",code="200",client_id="1999"}'),
    ('http_requests_total{status_code=~"2.*"}', 'http_requests_total{status_code=~"2.*",client_id="1999"}'),
    ('http_requests_total[5h]','http_requests_total{client_id="1999"}[5h]'),
    ('http_requests_total[5m]','http_requests_total{client_id="1999"}[5m]'),
    ('http_requests_total{status_code=~"2.*"}[50h]','http_requests_total{status_code=~"2.*",client_id="1999"}[50h]'),
    ('http_requests_total{job="apiserver",handler="/api/comments"}','http_requests_total{job="apiserver",handler="/api/comments",client_id="1999"}'),
    ('http_requests_total{job="apiserver",handler="/api/comments"}[5m]','http_requests_total{job="apiserver",handler="/api/comments",client_id="1999"}[5m]'),
    ('http_requests_total{job=~".*server"}','http_requests_total{job=~".*server",client_id="1999"}'),
    ('http_requests_total{status!~"4.."}','http_requests_total{status!~"4..",client_id="1999"}'),
    ('rate(http_requests_total[5m])','rate(http_requests_total{client_id="1999"}[5m])'),
    ('irate(http_requests_total[5m])','irate(http_requests_total{client_id="1999"}[5m])'),
    ('increase(http_requests_total{status_code=~"2.*"})','increase(http_requests_total{status_code=~"2.*",client_id="1999"})'),
    ('sum(rate(http_requests_total[5m]))','sum(rate(http_requests_total{client_id="1999"}[5m]))'),
    ('sum(rate(http_requests_total[5m] offset 5m))','sum(rate(http_requests_total{client_id="1999"}[5m] offset 5m))'),
    ('rate(http_requests_total[5m])[30m:1m]','rate(http_requests_total{client_id="1999"}[5m])[30m:1m]'),
    ('max_over_time(deriv(rate(distance_covered_total[5s])[30s:5s])[10m:])','max_over_time(deriv(rate(distance_covered_total{client_id="1999"}[5s])[30s:5s])[10m:])'),
    ('sum(http_requests_total{method="GET"} offset 5m)','sum(http_requests_total{method="GET",client_id="1999"} offset 5m)'),
    ('100*100','100*100'),
    ('12.1*http_requests_total[1h]','12.1*http_requests_total{client_id="1999"}[1h]'),
    ('http_requests_total[5h]*http_requests_total[1h]','http_requests_total{client_id="1999"}[5h]*http_requests_total{client_id="1999"}[1h]'),
    ('100>100','100>100'),
    ('12.1!=http_requests_total[1h]','12.1!=http_requests_total{client_id="1999"}[1h]'),
    ('http_requests_total[5h]==http_requests_total{status_code=~"2.*"}[50h]','http_requests_total{client_id="1999"}[5h]==http_requests_total{status_code=~"2.*",client_id="1999"}[50h]'),
    ('node_filesystem_avail{fstype!~"tmpfs|fuse.lxcfs|squashfs"}/node_filesystem_size{fstype!~"tmpfs|fuse.lxcfs|squashfs"}',
        'node_filesystem_avail{fstype!~"tmpfs|fuse.lxcfs|squashfs",client_id="1999"}/node_filesystem_size{fstype!~"tmpfs|fuse.lxcfs|squashfs",client_id="1999"}'),
    ('rate(http_requests_total{status_code=~"5.*"}[5m])/rate(http_requests_total[5m])',
        'rate(http_requests_total{status_code=~"5.*",client_id="1999"}[5m])/rate(http_requests_total{client_id="1999"}[5m])'),
    ('rate(http_requests_total{status_code=~"5.*"}[5m])>.1*rate(http_requests_total[5m])',
        'rate(http_requests_total{status_code=~"5.*",client_id="1999"}[5m])>.1*rate(http_requests_total{client_id="1999"}[5m])'),
    ('max_over_time(deriv(rate(distance_covered_total[5s])[30s:5s])[10m:])and http_requests_total{status_code=~"2.*"}[50h]',
        'max_over_time(deriv(rate(distance_covered_total{client_id="1999"}[5s])[30s:5s])[10m:])and http_requests_total{status_code=~"2.*",client_id="1999"}[50h]'),
    ('method_code:http_errors:rate5m{code="500"}','method_code:http_errors:rate5m{code="500",client_id="1999"}'),
    ('method_code.http_errors.rate5m{code.isgood="500"}','method_code.http_errors.rate5m{code.isgood="500",client_id="1999"}'),
    ('sum without(instance)(http_requests_total)','sum without(instance)(http_requests_total{client_id="1999"})'),
    ('sum by(application,group)(http_requests_total)','sum by(application,group)(http_requests_total{client_id="1999"})'),
    ('sum(http_requests_total)','sum(http_requests_total{client_id="1999"})'),
    ('count_values("version",build_version)','count_values("version",build_version{client_id="1999"})'),
    ('topk(5,http_requests_total)','topk(5,http_requests_total{client_id="1999"})'),
    ('sum(http_requests_total)by(application,group)','sum(http_requests_total{client_id="1999"})by(application,group)'),
    ('sum(http_requests_total)without(instance)','sum(http_requests_total{client_id="1999"})without(instance)'),
    ('rate(http_requests_total{status_code=~"5.*",client_id="123"}[5m])>.1*rate(http_requests_total{client_id="123"}[5m])',
     'rate(http_requests_total{status_code=~"5.*",client_id="1999"}[5m])>.1*rate(http_requests_total{client_id="1999"}[5m])'),
    ('(instance_memory_limit_bytes-instance_memory_usage_bytes)/1024/1024',
     '(instance_memory_limit_bytes{client_id="1999"}-instance_memory_usage_bytes{client_id="1999"})/1024/1024'),
    ('sum by(app,proc)(instance_memory_limit_bytes-instance_memory_usage_bytes)/1024/1024',
     'sum by(app,proc)(instance_memory_limit_bytes{client_id="1999"}-instance_memory_usage_bytes{client_id="1999"})/1024/1024')
]

denied_samples = [
    'sum without(client_id)(http_requests_total)',
    'sum(http_requests_total)without(client_id)',
    'sum(http_requests_total{method="GET"} ofefset 5m)',
    'sum(sort_desc(sum_over_time(ALERTS{alertstate="firing"}[24h])))without(alertname,client_id)'
]

whitelist = [
    'common_system_metric1',
    'common_system_metric2'
]

whitelist_samples = [
    ('common_system_metric1', 'common_system_metric1'),
    ('common_system_metric2{job="prometheus",code="200"}','common_system_metric2{job="prometheus",code="200"}'),
    ('(instance_memory_limit_bytes-common_system_metric2)/1024/1024',
     '(instance_memory_limit_bytes{client_id="1999"}-common_system_metric2)/1024/1024'),
]


@pytest.mark.parametrize(('param1'), promql_samples)
def test_parser(parser, param1):
    tree = parser.parse(param1)
    assert tree
    print()
    print(param1)
    print(tree.pretty())


@pytest.mark.parametrize(('param1'), promql_samples)
def test_reconstructor(parser, param1):
    tree = parser.parse(param1)
    sample2 = parser.reconstruct(tree)
    print()
    print(param1)
    print(sample2)
    assert (param1 == sample2)


@pytest.mark.parametrize(('param1', 'param2'), rewriter_samples)
def test_rewriter(rewriter, param1, param2):
    rev = rewriter.rewrite(param1, '1999')
    print()
    print(f'original: {param1}')
    print(f'expected: {param2}')
    print(f'rewrited: {rev}')
    assert (param2 == rev)

@pytest.mark.parametrize(('param1'), denied_samples)
def test_rewriter_denial(rewriter, param1):
    catched = False
    try:
        rev = rewriter.rewrite(param1, '1999')
    except AuthError as e:
        print(e)
        catched = True
    assert catched


@pytest.mark.parametrize(('param1', 'param2'), whitelist_samples)
def test_rewriter_whitelists(whitelisted_rewriter, param1, param2):
    rev = whitelisted_rewriter.rewrite(param1, '1999')
    print()
    print(f'original: {param1}')
    print(f'expected: {param2}')
    print(f'rewrited: {rev}')
    assert (param2 == rev)