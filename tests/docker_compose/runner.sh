#!/usr/bin/env bash

# Print commands
set -x

export VMETRICS_DATA=/home/rho/data/victoriametrics_data
export GRAFANA_DATA=/home/rho/data/grafana_data
export PROMETHEUS_OAUTH_PROXY=registry.gitlab.com/optima_public/prometheus_oauth_proxy:0.1.2

docker_compose_cmd="docker-compose -f ./docker-compose.yml"

${docker_compose_cmd} up