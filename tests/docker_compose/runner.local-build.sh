#!/usr/bin/env bash

# Print commands
set -x

export VMETRICS_DATA=/home/rho/data/victoriametrics_data
export GRAFANA_DATA=/home/rho/data/grafana_data

docker_compose_cmd="docker-compose -f ./docker-compose.local-build.yml"

${docker_compose_cmd} build
${docker_compose_cmd} up
