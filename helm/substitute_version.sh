#!/usr/bin/env bash

scriptDir=$(dirname -- "$(readlink -f -- "$BASH_SOURCE")")

if [ -z $CI_COMMIT_TAG ]; then
    echo "ERROR: variable CI_COMMIT_TAG is unset";
    exit 1
fi

sed -i '/  name\: registry.gitlab.com\/optima_public\/prometheus_oauth_proxy/c\  name\: registry.gitlab.com\/optima_public\/prometheus_oauth_proxy\:'$CI_COMMIT_TAG'' "${scriptDir}"/prometheus-oauth-proxy/values.yaml
sed -i '/version\:/c\version\: '$CI_COMMIT_TAG'' "${scriptDir}"/prometheus-oauth-proxy/Chart.yaml